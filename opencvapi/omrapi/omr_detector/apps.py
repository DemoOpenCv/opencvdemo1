from django.apps import AppConfig


class OmrDetectorConfig(AppConfig):
    name = 'omr_detector'
