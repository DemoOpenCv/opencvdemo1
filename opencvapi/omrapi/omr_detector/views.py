from django.shortcuts import render

# Create your views here.
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from imutils import contours
import numpy as np
import argparse
import imutils
import cv2
import os
import glob
import urllib
import os, os.path
from os import listdir

def sort_contours(cnts, method="top-to-bottom"):
	# initialize the reverse flag and sort index
	reverse = False
	i = 0

	# handle if we need to sort in reverse
	if method == "right-to-left" or method == "bottom-to-top":
		reverse = True

	# handle if we are sorting against the y-coordinate rather than
	# the x-coordinate of the bounding box
	if method == "top-to-bottom" or method == "bottom-to-top":
		i = 1

	# construct the list of bounding boxes and sort them from top to
	# bottom
	boundingBoxes = [cv2.boundingRect(c) for c in cnts]
	(cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
		key=lambda b:b[1][i], reverse=reverse))

	# return the list of sorted contours and bounding boxes
	return (cnts, boundingBoxes)

def sort_line_list(lines):
    # sort lines into horizontal and vertical
    vertical = []
    horizontal = []
    for line in lines:
        if line[0] == line[2]:
            vertical.append(line)
        elif line[1] == line[3]:
            horizontal.append(line)
    vertical.sort()
    horizontal.sort(key=lambda x: x[1])
    return horizontal, vertical

def draw_contour(image, c, i):
	# compute the center of the contour area and draw a circle
	# representing the center
	M = cv2.moments(c)
	cX = int(M["m10"] / M["m00"])
	cY = int(M["m01"] / M["m00"])

	# draw the countour number on the image
	cv2.putText(image, "{}".format(i), (cX-20, cY+10), cv2.FONT_HERSHEY_SIMPLEX,1.0, (34, 153, 84))

	# return the image with the contour number drawn on it
	return image, M

start_patterns = ('aaa')
end_patterns = ('bbb')

def getrawdata(valuelist):
    datafile = open("guru.txt","r")
    count=0
    numb=0
    #valuelist=[]
    section_in_play = False
    for i,line in enumerate(datafile):
        if line.startswith(start_patterns):
            section_in_play = True
        elif line.startswith(end_patterns):
            section_in_play = False
            #f=open("value.txt", "a+")
            #f.write("%s \n" %(count))
            valuelist.append(count)
            count=0
        if section_in_play:
            count=count+1
    datafile.close()
    return count



#image = [cv2.imread(file) for file in glob.glob("D:/PyOpencv/testpy/T1/T1/*.jpg")]
#image = cv2.imread("ex1.jpg")
#ratio = len(image[0]) / 500.0


def processimage():
    original_image = image.copy()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    cv2.imshow ("ori", image)
    cv2.imshow("gray", gray)

    blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    edged = cv2.Canny(gray, 50, 200,apertureSize=3)
    #lines = cv2.HoughLinesP(edged, 1, np.pi/180, 200, minLineLength=20, maxLineGap=999)[0].tolist()


    thresh = cv2.adaptiveThreshold(gray, 255,cv2.ADAPTIVE_THRESH_MEAN_C , cv2.THRESH_BINARY, 15, -2)
    #(thresh, img_bin) = cv2.threshold(image, 128, 255,cv2.THRESH_BINARY| cv2.THRESH_OTSU)
    thresh = 255-thresh
    cv2.imshow ("thresh", thresh)

    horizontal = np.copy(thresh)
    vertical = np.copy(thresh)

    # [horiz]
    cols = horizontal.shape[1]
    horizontal_size = (cols) // 20
    horizontalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (horizontal_size, 1))
    horizontal = cv2.erode(horizontal, horizontalStructure)
    horizontal = cv2.dilate(horizontal, horizontalStructure)

    # [vert]
    rows = vertical.shape[0]
    verticalsize = rows // 20
    verticalStructure = cv2.getStructuringElement(cv2.MORPH_RECT, (1, verticalsize))
    vertical = cv2.erode(vertical, verticalStructure)
    vertical = cv2.dilate(vertical, verticalStructure)

    # A kernel of (3 X 3) ones.
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))

    alpha = 0.5
    beta = 1.0 - alpha
    final_bin = cv2.addWeighted(vertical, alpha, horizontal, beta, 0.0)
    final_bin = cv2.erode(~final_bin, kernel, iterations=2)
    (thresh, final_bin) = cv2.threshold(final_bin, 127,255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
    cv2.imshow("finalbin.jpg",final_bin)

    ## Find contours for image, which will detect all the boxes
    ( _,cnts, hierarchy) = cv2.findContours(final_bin, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    #cnts = sorted(cnts, key=cv2.contourArea, reverse=False)

    ## Sort all the contours by top to bottom.
    #(cnts, boundingBoxes) = sort_contours(cnts)
    if os.path.exists("guru.txt"):
        os.remove("guru.txt")
    f=open("guru.txt", "a+")
    for (i, c) in enumerate(cnts):
        (_,a)=draw_contour(original_image, c, i)

        f.write("aaa\n %s\nbbb\n" %(c))

    f.close()
    valuelist=list()
    getrawdata(valuelist)
    finallist=list()
    lenvaluelist = (len(valuelist)-1)
    for x in range(3,lenvaluelist):
        #print(valuelist[x])
        y=x+1
        #print(valuelist[y])
        if (valuelist[x]==valuelist[y]):
           finallist.append(1)
        elif(valuelist[x]==16):
            finallist.append(0)

    #print(valuelist)
    #print(finallist)

    lenfinalist1= len(finallist)
    for k in range(12,lenfinalist1,12):
        j=k+2
        del finallist[k:j]

    del finallist[240:(lenfinalist1+1)]
    finallist.reverse()
    #print(finallist)
	#return JsonResponse(finallist)
    #print (len(finallist))

    #show the original, unsorted contour image
    cv2.imshow("Sorted", original_image)

    #idx = 0
    #for c in cnts:
    #    # Returns the location and width,height for every contour
    #    x, y, w, h = cv2.boundingRect(c)
    #    if (w > 80 and h > 20) and w > 3*h:
    #        idx += 1
    #        new_img = original_image[y:y+h, x:x+w]
    #        cv2.imwrite(str(idx) + '.jpg', new_img)
    ## If the box height is greater then 20, widht is >80, then only save it as a box in "cropped/" folder.
    #    if (w > 80 and h > 20) and w > 3*h:
    #        idx += 1
    #        new_img = original_image[y:y+h, x:x+w]
    #        cv2.imwrite("a"+str(idx) + '.jpg', new_img)
    cv2.waitKey(25)

path = os.path.join(os.getcwd(), 'imgs')
print (path)

@csrf_exempt
def detect(request):
	data = {"success": False}
	images = [cv2.imread(file) for file in glob.glob("D:/PyOpencv/testpy/T1/T1/tt/*.jpg")]
	for image in images:
	    processimage()
	#cv2.waitKey(0)
	data["success"] = True
	return JsonResponse(data)
