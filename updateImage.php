<!DOCTYPE html>
<html>
    <head>
        <title>Attendance website demo</title>
        <link rel="stylesheet" type="text/css" href="css/main.css">
    </head>
    <body>
<?php
$errors = array();
$uploadedFiles = array();
$extension = array("jpeg","jpg","png");
$bytes = 1024;
$KB = 1024;
$MB=5;
//maximum image size will be 5MB
$totalBytes =$MB* $bytes * $KB;
$UploadFolder = "images";
 
$counter = 0;
foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name){
    $temp = $_FILES["files"]["tmp_name"][$key];
    $name = $_FILES["files"]["name"][$key];
     
    if(empty($temp))
    {
        break;
    }
     
    $counter++;
    $UploadOk = true;
     
    if($_FILES["files"]["size"][$key] > $totalBytes)
    {
        $UploadOk = false;
        array_push($errors, $name." file size is larger than the 5 MB.");
    }
     
    $ext = pathinfo($name, PATHINFO_EXTENSION);
    if(in_array($ext, $extension) == false){
        $UploadOk = false;
        array_push($errors, $name." is invalid file type.");
    }

    if(!file_exists($UploadFolder)) {
        mkdir($UploadFolder, 0777);
        if(file_exists($UploadFolder."/".$name) == true){
            $UploadOk = false;
            array_push($errors, $name." file is already exist.");
        }
         
        if($UploadOk == true){
            move_uploaded_file($temp,$UploadFolder."/".$name);
            array_push($uploadedFiles, $name);
        }

    } else {
        if(file_exists($UploadFolder."/".$name) == true){
            $UploadOk = false;
            array_push($errors, $name." file is already exist.");
        }
         
        if($UploadOk == true){
            move_uploaded_file($temp,$UploadFolder."/".$name);
            array_push($uploadedFiles, $name);
        }
    }

}
 
if($counter>0){
    if(count($errors)>0)
    {
        echo "<div class='errorAlert'><b>Errors:</b>";
        echo "<br/><ul>";
        foreach($errors as $error)
        {
            echo "<li>".$error."</li>";
        }
        echo "</ul></div><br/><a href='index.html'> <-Back. </a>";
    }
     
    if(count($uploadedFiles)>0){
        echo "<div class='successAlert'><b>Uploaded Files:</b>";
        echo "<br/><ul>";
        foreach($uploadedFiles as $fileName)
        {
            echo "<li>".$fileName."</li>";
        }
        echo "</ul><br/><b>";
         
        echo count($uploadedFiles)." file(s)</b> is/are successfully uploaded.</div> <a href='index.html'> <-Back. </a>";
    }                               
}
else{
    echo "Please, Select file(s) to upload.<a href='index.html'> <-Back. </a>";
}
?>
</body>