# Attendence website

The Project will detect image to import data in system.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them:
You need to install:

Xampp https://www.apachefriends.org/index.html

### Set up

1) And then you should clone the project.

2) Next copy this folder to DISK:\xampp\htdocs.

3) Then open xampp control panel click "Start" button in both Apache and MySQL.

4) Access http://localhost/{folder Project Name}/index.html

